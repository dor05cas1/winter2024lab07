import java.util.Random;
public class Deck
{
	//fields
	private Card [] cards;
	private int numberOfCards;
	private Random rng;
	//constructor
	public Deck()
	{
		this.rng = new Random(53);
		this.cards = new Card[52];
		this.numberOfCards = 52;
		
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", 
								"Nine", "Ten", "Jack", "Queen", "King"};
		String[] suit = {"Spades", "Clubs", "Hearts", "Diamonds"};
		
		int index = 0;
		for (int i = 0; i < values.length; i++)
		{
			for (int j = 0; j < suit.length; j++)
			{
				this.cards[index] = new Card(suit[j], values[i]);
				index++;
				
			}
		}
		
	}
	
	//toString
	public String toString(){
		String cardList = "";
		for (int i = 0; i < this.numberOfCards; i ++)
		{
			cardList += this.cards[i] + "\n";
		}
		return cardList;
	}
	//instance methods
	public int length()
	{
		return this.numberOfCards;
	}
	
	public Card drawTopCard()
	{
		this.numberOfCards --;
		return this.cards[this.numberOfCards];
	}
	
	public void shuffle()
	{
		for (int i = 0; i < this.numberOfCards; i++)
		{
			int index = i + rng.nextInt(this.numberOfCards - i);
			Card swap = this.cards[i];
			this.cards[i] = this.cards[index];
			this.cards[index] = swap;
		}
		
	}



}